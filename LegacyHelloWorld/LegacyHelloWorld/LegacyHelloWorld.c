#include "LegacyHelloWorld.h"

NTSTATUS DriverEntry(PDRIVER_OBJECT pDriver, PUNICODE_STRING pusRegistry)
{
	NTSTATUS ntRet = STATUS_SUCCESS;

	MyDbgPrint("Loading LegacyHelloWorld Driver\n");
	MyDbgPrint("Driver Registy Key: %wZ\n", pusRegistry);

	LegacyHelloWorld();
	pDriver->DriverUnload = DriverUnload;

	return ntRet;
}

VOID DriverUnload(PDRIVER_OBJECT pDriver)
{
	UNREFERENCED_PARAMETER(pDriver);
	MyDbgPrint("Unloading LegacyHelloWorld Driver\n");
}

VOID LegacyHelloWorld(VOID)
{
	MyDbgPrint("Hello from the LegacyHelloWorld Driver\n");
}