#pragma once
#include <ntddk.h>

#define MyDbgPrint(fmt, ...) DbgPrint("[DEBUG] -- " ## fmt, __VA_ARGS__)

NTSTATUS DriverEntry(PDRIVER_OBJECT, PUNICODE_STRING);
VOID DriverUnload(PDRIVER_OBJECT);
VOID LegacyHelloWorld(VOID);
