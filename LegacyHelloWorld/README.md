# win10-helloworld-software-driver

A simple hello world driver for windows 10 using the legacy NT driver Model.

## Project Creation
1. Create a new `Empty WDM Driver` Project
2. Add a new `.c` file and include `ntddk.h`
3. In project properties -> General: Set windows SDK version to 10.0.17134.0

## Debugger Setup
1. Create a windows 10 VM
2. Use kdnet.exe to set up debugging

# Scripts
1. Use batch scripts to help with running after reverts